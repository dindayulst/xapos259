package com.xapos259;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Xapo259Application {

	public static void main(String[] args) {
		SpringApplication.run(Xapo259Application.class, args);
	}

}
