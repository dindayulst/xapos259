package com.xapos259.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.xapos259.model.OrderDetail;
import com.xapos259.repository.OrderDetailRepository;


@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/api/")
public class ApiOrderDetail {
	
	@Autowired
	private OrderDetailRepository orderDetailRepository;

	@GetMapping("order_detail")
	@PreAuthorize("hasRole('USER')")
	public ResponseEntity<List<OrderDetail>> getAllOrderDetail() {
		try {
			List<OrderDetail> orderDetail = this.orderDetailRepository.findAll();
			return new ResponseEntity<>(orderDetail, HttpStatus.OK);
		} catch (Exception err) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
	}
	
	@GetMapping("order_detail/{id}")
	@PreAuthorize("hasRole('USER')")
	public ResponseEntity<List<OrderDetail>> getOrderDetailById(@PathVariable("id") Long id){
		try {
			Optional<OrderDetail> orderDetail = this.orderDetailRepository.findById(id);
			if (orderDetail.isPresent()) {
				ResponseEntity rest = new ResponseEntity<>(orderDetail, HttpStatus.OK);
				return rest;
			} else {
				return ResponseEntity.notFound().build();
			}
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
	}
	
	@PostMapping("order_detail")
	@PreAuthorize("hasRole('USER')")
	public ResponseEntity<Object> saveOrderDetail(@RequestBody OrderDetail orderDetail) {
		OrderDetail orderDetailData = this.orderDetailRepository.save(orderDetail);
		if (orderDetailData.equals(orderDetail)) {
			return new ResponseEntity<>("Save Success", HttpStatus.OK);
		} else {
			return new ResponseEntity<>("Save Failed", HttpStatus.BAD_REQUEST);
		}
	}
	
	@PutMapping("order_detail")
	@PreAuthorize("hasRole('USER')")
	public ResponseEntity<Object> updateOrderDetail(@RequestBody OrderDetail orderDetail) {
		Long id = orderDetail.getId();
		Optional<OrderDetail> orderDetailData = this.orderDetailRepository.findById(id);
		if (orderDetailData.isPresent()) {
			orderDetail.setId(id);
			this.orderDetailRepository.save(orderDetail);
			return new ResponseEntity<>("Update Success", HttpStatus.OK);
		} else {
			return ResponseEntity.notFound().build();
		}
	}
	
	@DeleteMapping("order_detail/{id}")
	@PreAuthorize("hasRole('USER')")
	public ResponseEntity<Object> deleteOrderDetail(@PathVariable("id") Long id){
		this.orderDetailRepository.deleteById(id);
		return new ResponseEntity<>("Delete Success", HttpStatus.OK);
	}

}
