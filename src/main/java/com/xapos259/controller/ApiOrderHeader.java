package com.xapos259.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.xapos259.model.OrderHeader;
import com.xapos259.repository.OrderHeaderRepository;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/api/")
public class ApiOrderHeader {
	
	@Autowired
	private OrderHeaderRepository orderHeaderRepository;

	@GetMapping("order_header")
	@PreAuthorize("hasRole('USER')")
	public ResponseEntity<List<OrderHeader>> getAllOrderHeader() {
		try {
			List<OrderHeader> orderHeader = this.orderHeaderRepository.findAll();
			return new ResponseEntity<>(orderHeader, HttpStatus.OK);
		} catch (Exception err) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
	}
	
	@GetMapping("order_header/{id}")
	@PreAuthorize("hasRole('USER')")
	public ResponseEntity<List<OrderHeader>> getOrderHeaderById(@PathVariable("id") Long id){
		try {
			Optional<OrderHeader> orderHeader = this.orderHeaderRepository.findById(id);
			if (orderHeader.isPresent()) {
				ResponseEntity rest = new ResponseEntity<>(orderHeader, HttpStatus.OK);
				return rest;
			} else {
				return ResponseEntity.notFound().build();
			}
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
	}
	
	@PostMapping("order_header")
	@PreAuthorize("hasRole('USER')")
	public ResponseEntity<Object> saveOrderHeader(@RequestBody OrderHeader orderHeader) {
		OrderHeader orderHeaderData = this.orderHeaderRepository.save(orderHeader);
		if (orderHeaderData.equals(orderHeader)) {
			return new ResponseEntity<>("Save Success", HttpStatus.OK);
		} else {
			return new ResponseEntity<>("Save Failed", HttpStatus.BAD_REQUEST);
		}
	}
	
	@PutMapping("order_header")
	@PreAuthorize("hasRole('USER')")
	public ResponseEntity<Object> updateOrderHeader(@RequestBody OrderHeader orderHeader) {
		Long id = orderHeader.getId();
		Optional<OrderHeader> orderHeaderData = this.orderHeaderRepository.findById(id);
		if (orderHeaderData.isPresent()) {
			orderHeader.setId(id);
			this.orderHeaderRepository.save(orderHeader);
			return new ResponseEntity<>("Update Success", HttpStatus.OK);
		} else {
			return ResponseEntity.notFound().build();
		}
	}
	
	@DeleteMapping("order_header/{id}")
	@PreAuthorize("hasRole('USER')")
	public ResponseEntity<Object> deleteOrderHeader(@PathVariable("id") Long id){
		this.orderHeaderRepository.deleteById(id);
		return new ResponseEntity<>("Delete Success", HttpStatus.OK);
	}

}
