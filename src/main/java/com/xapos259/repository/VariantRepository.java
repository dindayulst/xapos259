package com.xapos259.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.xapos259.model.Variant;

@Repository
public interface VariantRepository extends JpaRepository<Variant, Long>{
	@Query("FROM Variant WHERE CategoryId = ?1")
	List<Variant> findByCategoryId(Long CategoryId);
}
